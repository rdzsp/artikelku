<?php

use App\Http\Controllers\SiteController;
use App\Http\Controllers\CustomAuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Route::post('signin', [CustomAuthController::class, 'signIn'])->name('signin');
Route::post('signup', [CustomAuthController::class, 'signUp'])->name('signup');
Route::get('signout', [CustomAuthController::class, 'signOut'])->name('signout');

//Protect this with middleware
Route::get('/home', [SiteController::class, 'index'])->name('home');
Route::redirect('/articles', '/home');

Route::middleware('is_login')->group(function () {
    Route::get('/articles/mine', [SiteController::class, 'myArticles'])->name('my_articles');
    Route::match(['get', 'post'], '/articles/new', [SiteController::class, 'newArticles'])->name('new_article');
    Route::get('/articles/delete/{id}', [SiteController::class, 'deleteArticles'])->name('delete_article');

    Route::post('/comments/new/{id_article}', [SiteController::class, 'newComment'])->name('new_comment');
    Route::get('/comments/delete/{id}', [SiteController::class, 'deleteComment'])->name('delete_comment');
    Route::match(['get', 'patch'], '/articles/update/{id}', [SiteController::class, 'updateArticles'])->name('update_article');
});

Route::get('/articles/{id}', [SiteController::class, 'getArticles'])->name('get_article');
