@extends('layouts.app')

@section('title', 'View Articles')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-9">
            <img class="img-fluid rounded shadow" width="100%" src="https://upload.wikimedia.org/wikipedia/commons/7/75/No_image_available.png">
            <h1 class="m-2">{{ $data->title }}</h1>
            <h6 class="text-muted"><span class="fa fa-clock"></span> {{ Carbon\Carbon::parse($data->created_at)->format('D, d M Y H:i') }} | <span class="fa fa-user"></span> {{ $data->authors_by_author->name }}</h6>
            <hr>
            <p class="content">{!! $data->content !!}</p>

            <hr class="mt-5">
            <div class="comments-section">
                <h4><span class="fa fa-comment"></span> Comments</h4>
                <div class="comments mb-4">
                    <div class="border">
                        <div class="p-4" style="max-height: 300px; overflow-y: scroll;">
                            {{ count($data->comments_by_article) < 1 ? 'No Comment':'' }}
                            @foreach($data->comments_by_article as $comment)
                            <div class="m-3 row">
                                <div class="user-info col-sm-5">
                                    <img class="img-circle" width="30px" src="https://freepikpsd.com/media/2019/10/user-png-image-9.png"> {{ $comment->author }}
                                </div>
                                <div class="col-sm-6">
                                    : {{ $comment->content }}
                                </div>
                                @if($comment->author == Session::get('user')['name'] || $data->author == Session::get('user')['id'])
                                <div class="col-sm-1">
                                    <a href="{{route('delete_comment', $comment->id)}}"><span class="fa fa-trash text-danger"></span></a>
                                </div>
                                @endif
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <form method="POST" action="{{ route('new_comment', $data->id) }}">
                        @csrf
                        <textarea class="form-control" name="frm-comment" id="exampleFormControlTextarea1" rows="3"></textarea>
                        <div class="d-grid gap-2">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection