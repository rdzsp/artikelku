@extends('layouts.app')

@section('title', 'My Articles')

@section('content')
@include('layouts._partial.jumbotron')
<div class="container">
    <div class="row">
        <div class="col-sm-9">
            <div class="row">
                @foreach ($data as $item)
                <div class="col-sm-4 mt-4">
                    <div class="card">
                        <div class="card-body">
                            <img class="img-fluid" width="100%" src="http://ideas.or.id/wp-content/themes/consultix/images/no-image-found-360x250.png" width="150px">
                            <h3 class="card-title"><span title="{{ $item->title }}">{{ Str::limit($item->title, 10) }}</span></h3>
                            <small class="card-subtitle mb-2 text-muted"><span class="fa fa-clock"></span> {{ Carbon\Carbon::parse($item->created_at)->format('D, d M Y') }} | <span class="fa fa-user"></span> <span title="{{ $item->authors_by_author->name }}">{{ Str::limit($item->authors_by_author->name, 5) }}</span></small>
                            <p class="card-text">
                                {{ Str::limit(strip_tags($item->content), 5, '...') }}
                                <a href="{{ url('/articles/'. $item->id) }}"><small>Read More</small></a>
                            </p>
                            <a href="{{ route('update_article', $item->id) }}" class="btn btn-block btn-primary "><span class="fa fa-edit"></span> Edit</a>
                            <a href="{{ route('delete_article', $item->id) }}" class="btn btn-block btn-danger"><span class="fa fa-trash"></span> Delete</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection