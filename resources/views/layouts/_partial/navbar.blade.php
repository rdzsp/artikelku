<nav class="navbar navbar-expand-lg navbar-dark bg-main-color">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('home') }}">ArtikelKu</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('home') ? 'active':'' }}" aria-current="page" href="{{ route('home') }}"><span class="fa fa-home"></span> Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('new_article') ? 'active':'' }}" href="{{ route('new_article') }}"><span class="fa fa-plus"></span> Add Article</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('new_article') ? 'active':'' }}" href="{{ route('my_articles') }}"><span class="fa fa-newspaper"></span> My Articles</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <span class="fa fa-user"></span> {{ Session::has('user') ? Session::get('user')['name'] : 'You don\'t have an account' }}
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown" style="right: 0; left: auto;">
                        @if(!Session::has('user'))
                        <li><a class="dropdown-item" href="#" data-bs-toggle="modal" data-bs-target="#modalLogin">Login</a></li>
                        <li><a class="dropdown-item" href="#" data-bs-toggle="modal" data-bs-target="#modalRegister">Register</a></li>
                        @else
                        <!-- <li><a class="dropdown-item" href="#">Edit Account</a></li> -->
                        <li><a class="dropdown-item" href="{{ route('signout') }}">Logout</a></li>
                        @endif
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>