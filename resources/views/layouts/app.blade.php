<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ArtikelKu | @yield('title')</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ mix('css/styles.css') }}">
</head>

<body>
    @if($errors->has('must_login'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ $errors->first('must_login') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    @if($errors->has('error_login'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ $errors->first('error_login') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif

    @include('layouts._partial.navbar')

    @yield('content')

    @include('layouts._partial.modal_login')
    @include('layouts._partial.modal_register')

    @section('page-script')
    <script src="{{ mix('js/app.js') }}"></script>
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', (event) => {
            tinymce.init({
                selector: 'textarea#frm-content',
                content_css: false,
                skin: false
            });
        });
    </script>
    @show
</body>

</html>