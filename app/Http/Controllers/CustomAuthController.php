<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class CustomAuthController extends Controller
{
    const API_BASE = 'https://blog-api.stmik-amikbandung.ac.id/api/v2/blog/_table/';
    const API_KEY = 'ef9187e17dce5e8a5da6a5d16ba760b75cadd53d19601a16713e5b7c4f683e1b';
    private $apiClient;

    public function __construct()
    {
        $this->apiClient = new Client([
            'base_uri' => self::API_BASE,
            'headers' => [
                'X-DreamFactory-API-Key' => self::API_KEY
            ]
        ]);
    }
    
    public function signIn(Request $request) {
        if($request->isMethod('post')) {
            $name = $request->input('name');
            $email = $request->input('email');
            $reqData = $this->apiClient->get('authors', [
                'query' => [
                    'filter'=>"name={$name}",
                    'filter'=>"email={$email}"
                ]
            ]);
            $apiResponse = json_decode($reqData->getBody())->resource;
            $countUser = count($apiResponse);
            if($countUser > 0){
                $request->session()->forget('user');
                $request->session()->put(['user'=>['id' => $apiResponse[0]->id,'name' => $name, 'email' => $email]]);
                return redirect('home');
            }

            return redirect('home/#error')->withErrors(['error_login'=>'LOGIN FAILED: Email or Name is invalid!']);
        }
    }

    public function signUp(Request $request) {
        if ($request->isMethod('post')) {
            $name = $request->input('name');
            $email = $request->input('email');

            $dataModel = [
                'resource' => []
            ];

            $dataModel['resource'][] = [
                'name' => $name,
                'email' => $email,
            ];

            try {
                $reqData = $this->apiClient->post('authors', [
                    'json' => $dataModel
                ]);
                $apiResponse = json_decode($reqData->getBody())->resource;

                // dd($apiResponse);
                Cache::forget('index');

                return redirect("home");
            } catch (Exception $e) {
                abort(404);
            }
        }
    }

    public function signOut() {
        session()->forget('user');

        return redirect('/home');
    }
}
