<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class SiteController extends Controller
{
    const API_BASE = 'https://blog-api.stmik-amikbandung.ac.id/api/v2/blog/_table/';
    const API_KEY = 'ef9187e17dce5e8a5da6a5d16ba760b75cadd53d19601a16713e5b7c4f683e1b';
    private $apiClient;

    public function __construct()
    {
        $this->apiClient = new Client([
            'base_uri' => self::API_BASE,
            'headers' => [
                'X-DreamFactory-API-Key' => self::API_KEY
            ]
        ]);
    }

    public function index()
    {
        $data = Cache::get('index_', function () {
            try {
                $reqData = $this->apiClient->get('articles', [
                    'query' => [
                        'related' => 'authors_by_author',
                        'order' => 'created_at DESC'
                    ]
                ]);
                $resource = json_decode($reqData->getBody())->resource;

                Cache::add('index', $resource);
                return $resource;
            } catch (Exception $e) {
                return [];
            }
        });

        return view('home', ['data' => $data]);
    }

    public function myArticles(Request $request)
    {
        $data = Cache::get('index_', function () use ($request) {
            try {
                $id_user = $request->session()->get('user')['id'];
                $reqData = $this->apiClient->get('articles', [
                    'query' => [
                        'filter' => "author={$id_user}",
                        'related' => 'authors_by_author',
                        'order' => 'created_at DESC'
                    ]
                ]);
                $resource = json_decode($reqData->getBody())->resource;

                Cache::add('mine', $resource);
                return $resource;
            } catch (Exception $e) {
                return [];
            }
        });

        return view('myArticles', ['data' => $data]);
    }

    public function getArticles($id)
    {
        $key = "articles/{$id}";
        $data = Cache::get($key . '_', function () use ($key) {
            try {
                $reqData = $this->apiClient->get($key, [
                    'query' => [
                        'related' => '*'
                    ]
                ]);
                $resource = json_decode($reqData->getBody());

                Cache::add($key, $resource);
                return $resource;
            } catch (Exception $e) {
                abort(404);
            }
        });

        return view('viewArticle', ['data' => $data]);
    }

    public function newArticles(Request $request)
    {
        if ($request->isMethod('post')) {
            $title = $request->input('frm-title');
            $content = $request->input('frm-content');
            $dataModel = [
                'resource' => []
            ];

            $dataModel['resource'][] = [
                'author' => $request->session()->get('user')['id'],
                'title' => $title,
                'content' => $content
            ];

            try {
                $reqData = $this->apiClient->post('articles', [
                    'json' => $dataModel
                ]);
                $apiResponse = json_decode($reqData->getBody())->resource;
                $newId = $apiResponse[0]->id;

                Cache::forget('index');

                return redirect("articles/{$newId}");
            } catch (Exception $e) {
                abort(404);
            }
        }

        return view('newArticle');
    }

    public function updateArticles(Request $request, $id)
    {
        $key = "articles/{$id}";
        Cache::forget($key);
        $data = Cache::get($key, function () use ($key, $request) {
            try {
                $id_user = $request->session()->get('user')['id'];
                $reqData = $this->apiClient->get($key);
                $resource = json_decode($reqData->getBody());
                if($resource->author != $id_user){
                    abort(404);
                }

                Cache::add($key, $resource);
                return $resource;
            } catch (Exception $e) {
                abort(404);
            }
        });

        if ($request->isMethod('patch')) {
            $ida = $request->input('idf');
            $title = $request->input('frm-title');
            $content = $request->input('frm-content');

            $dataModel = [
                'resource' => []
            ];
            $dataModel['resource'][] = [
                'id' => $ida,
                'title' => $title,
                'content' => $content
            ];

            $id_user = $request->session()->get('user')['id'];
            $reqData = $this->apiClient->get("articles/{$id}");
            $resource = json_decode($reqData->getBody());
            if($resource->author != $id_user){
                abort(404);
            }

            $reqData = $this->apiClient->patch("articles", [
                'json' => $dataModel
            ]);

            $resource = $dataModel['resource'][0];

            Cache::add($key, $resource);
            Cache::forget('index');
            Cache::forget($key);

            return redirect()->route('my_articles');
        }

        return view('updateArticle', ['data' => $data]);
    }

    public function deleteArticles($id)
    {
        $dataModel = [
            'resource' => []
        ];

        $dataModel['resource'][] = [
            'id' => $id
        ];
        $key = "articles/{$id}";
        try {
            $reqData = $this->apiClient->get("articles/{$id}", [
                'query' => [
                    'related' => 'comments_by_article'
                ]
            ]);
            $apiResponse = json_decode($reqData->getBody());
            $comments = $apiResponse->comments_by_article;
            foreach($comments as $comment){
                $dataModelComment = [
                    'resource' => []
                ];
        
                $dataModelComment['resource'][] = [
                    'id' => $comment->id
                ];
                $reqData = $this->apiClient->delete("comments/{$comment->id}", [
                    'json' => $dataModelComment
                ]);
            }
            
            $reqData = $this->apiClient->delete("articles/{$id}", [
                'json' => $dataModel
            ]);

            Cache::forget('index');
            Cache::forget($key);

            return redirect('home');
        } catch (Exception $e) {
            abort(404);
        }
    }

    public function newComment($id_article, Request $request)
    {
        if ($request->isMethod('post')) {
            $author = $request->session()->get('user')['name'];
            $comment = $request->input('frm-comment');

            $dataModel = [
                'resource' => []
            ];

            $dataModel['resource'][] = [
                'article' => $id_article,
                'author' => $author,
                'content' => $comment,
            ];

            try {
                $reqData = $this->apiClient->post('comments', [
                    'json' => $dataModel
                ]);

                return redirect()->route('get_article', $id_article);
            } catch (Exception $e) {
                abort(404);
            }
        }
    }

    public function deleteComment($id){
        $dataModel = [
            'resource' => []
        ];

        $dataModel['resource'][] = [
            'id' => $id
        ];
        $key = "comments/{$id}";
        try{
            $reqData = $this->apiClient->get($key);
            $apiResponse = json_decode($reqData->getBody());

            if($apiResponse->author != \Illuminate\Support\Facades\Session::get('user')['name']){
                return redirect()->route('get_article', $apiResponse->article); 
            }

            $reqData = $this->apiClient->delete($key, [
                'json' => $dataModel
            ]);

            return redirect()->route('get_article', $apiResponse->article);
        } catch (Exception $e){
            abort(404);
        }
    }
}
